const express = require('express');
const jsonServer = require('json-server');
const path = require('path');
const cors = require('cors');
const app = express();
const PORT = process.env.PORT || 3000;

const allowedOrigins = ['https://igvtech.online', 'http://localhost:5173', 'https://ws-dashboard-store.onrender.com'];
const corsOptions = {
  origin: function (origin, callback) {
    if (allowedOrigins.indexOf(origin) !== -1 || !origin) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
};

app.use(cors(corsOptions));

// Serve static files (optional)
app.use(express.static(path.join(__dirname, 'public')));

// JSON Server
const jsonServerRouter = jsonServer.router('./json-server/db.json'); // Assuming your JSON data is in a file named db.json
app.use('/api', jsonServerRouter); // Use a specific route for your JSON API

// Start the Express server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`); 
});